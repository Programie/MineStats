# MineStats

A Minecraft Bukkit plugin which sends statistics to InfluxDB.

[![pipeline status](https://gitlab.com/Programie/MineStats/badges/master/pipeline.svg)](https://gitlab.com/Programie/MineStats/commits/master)
[![download from GitLab](https://img.shields.io/badge/download-Releases-blue?logo=gitlab)](https://gitlab.com/Programie/MineStats/-/releases)

## Build

You can build the project in the following 2 steps:

 * Check out the repository
 * Build the jar file using maven: *mvn clean package*

**Note:** JDK 1.8 and Maven is required to build the project!